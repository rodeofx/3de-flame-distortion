# 3DE4.script.name:	Export Distortion LUT
# 3DE4.script.version:	v0.1

# 3DE4.script.gui:   	Main Window::XXX

# 3DE4.script.comment:	Exports distortion as a LUT to be used in extrernal appplications. Sampling matrix is 4px.
# Helder Tomas 08/04/2013

# TODO:
# If a shot doesn't make use of animated features (FL *AND* FD) I don't really need to export all of the frames, just one. (Add an 'animated' flag to the file)
# A smart person would work out if we really need EVERY frame, but I like shotgun approaches :)
# Sometimes plates aren't multiples of 4, DOPs love film gates, data-ops love to save disk-space, and wranglers love to save render-time...
# Overscan isn't a multiple of 4 never, ever, ever, ever, except on very rare exceptions, or until the tool writer decides to give it a hand...I might do that out of decency...
# Make sure there are no shameful bugs...

def exportDistortion():
	#Wich camera have we selected?
	cam = tde4.getCurrentCamera()
	if cam:
		#how many frames?
		frames = tde4.getCameraNoFrames(cam)

		# how big is our image?
		height = tde4.getCameraImageHeight(cam)
		width = tde4.getCameraImageWidth(cam)
		
		#how much is the x_dist and y_dist?
		x_dist = (1.0/width)
		y_dist = (1.0/height)
		
		#We want MIDDLE of pixels.
		x_start = (1.0/(width*2))
		y_start = (1.0/(height*2))

		if frames:
			#Now let's create our interface:
					path = tde4.postFileRequester("Export Distortion LUT...","*.dlut")
					if path!=None:
						#create the file
						f = open(path,"w")
						if not f.closed:

							#Prepare the initial overscan values, I'm starting with the original FOV as we never really want anything smaller than it, correct?.
							left = 0.0
							top = 1.0
							right = 1.0
							bottom = 0.0

							#Now cycle trough the rows and collumns and extract the information we want...
							for frame in range(frames):
								f.write('%s \n'%(frame+1))
								for y in range(height):
									#only need each 4th line
									line = [] #nice to clean each line
									if (y+1) % 4 == 0 or y == 0: #I add one to line up the lists, pixels start from 1, not 0
										for x in range(width):
											if (x+1) % 4 == 0 or x == 0: #I add one to line up the lists, pixels start from 1, not 0 and only need each 4th pixel

												#normalize values between 0 and 1 :)
												x_norm = (x+1) * x_dist - x_start												
												y_norm = (y+1) * y_dist - y_start
												
												ud_pos = tde4.removeDistortion2D(cam,frame+1,[x_norm,y_norm])
												line.append(ud_pos)

												# OVERSCAN USES THE MIDDLE OF THE PIXEL, NOT THE EDGES! I lost 10 minutes wondering why my results were off!
												if (x) == 0 and ud_pos[0] < left: left = ud_pos[0]
												if (y+1) == height and ud_pos[1] > top: top = ud_pos[1]
												if (x+1) == width and ud_pos[0] > right: right = ud_pos[0]
												if (y) == 0 and ud_pos[1] < bottom: bottom = ud_pos[1]
									if line:
										f.write(str(line)+'\n')
						f.write('#extremes:\n')
						f.write(str(left)+'\n')
						f.write(str(top)+'\n')
						f.write(str(right)+'\n')
						f.write(str(bottom)+'\n')

						f.close()

def main():
	exportDistortion()

main()
